
\begin{definition}
La \emph{fonction caractéristique} d'une variable réelle $X$ est la transformée de
\emph{Fourier} de sa loi de probabilité. On la note :
\begin{equation}
\varphi_X(t) = E[e^{itX}] \quad i \in \mathbb{C}, \; i^2 = -1
\end{equation}

\begin{description}
\item[Cas continu] $$\varphi_X(t) = \int_\mathbb{R} e^{itX}f(X)dX$$
\item[Cas discret] $$\varphi_X(t) = \sum_k e^{itk}P[X=k]$$
\end{description}
\end{definition}

\begin{propriete}
\begin{equation}
\varphi_{\lambda X}(t) = \varphi_X(\lambda t), \quad \forall \lambda \in \mathbb{C}
\end{equation}
\begin{equation}
\varphi_{X + a}(t) = e^{ita}\cdot\phi_X(t), \quad \forall a \in \mathbb{C}
\end{equation}
\end{propriete}

\begin{cons}
Si on pose $ U = \frac{X - m}{\sigma}, \quad \lambda = \frac{X}{\sigma}$ et $a = -m$, on obtient :
\begin{equation*}
\varphi_U(t) = e^{-\frac{itm}{\sigma}}\cdot \varphi_X(\frac{t}{\sigma})
\end{equation*}

Si $\frac{t}{\sigma} = v$ alors :
\begin{equation}
\varphi_X(v) = e^{ivm}\cdot\phi_U(\sigma v)
\end{equation}
\end{cons}

\begin{proposition}
\begin{align}
\varphi_X(0) &= 1 \label{prop:1}\\
\underbrace{\varphi_X^{(k)}(0)}_{\text{Dérivée d'ordre $k$}} &= i^k \cdot E[X^k] \label{prop:2}
\end{align}
\end{proposition}


\begin{preuve}
Supposons $X$ une variable aléatoire continue, l'expression de la fonction caractéristique est donc :
\begin{equation*}
\varphi_X(t) = \int_\mathbb{R} e^{itX}\cdot f(X)\cdot dX
\end{equation*}

On en déduit immédiatement,
\begin{align*}
\varphi_X(0) &= \int_\mathbb{R}f(X)\cdot dX \\
\varphi_X(0) &= 1 && \text{Car l'intégrale d'une densité vaut toujours $1$}
\end{align*}
La proposition (\ref{prop:1}) est donc vérifiée.

\paragraph{}
Afin de démontrer la proposition (\ref{prop:2}) on se propose de dériver l'expression de
la fonction caractéristique ci-dessus $k$ fois par rapport à $t$.

\paragraph{}
En dérivant une première fois on obtient :
\begin{align*}
\varphi'_X(t) &= \int_\mathbb{R}\frac{\partial}{\partial t}(e^{itX}\cdot f(X)) \cdot dX \\
&= \int_\mathbb{R}iXe^{itX}\cdot f(X)\cdot dX
\end{align*}

\paragraph{}
Si on dérive $k$ fois on obtient :
\begin{align*}
\varphi_X^{(k)}(t) &= \int_\mathbb{R}(iX)^ke^{itX}\cdot f(X)\cdot dX \\
\varphi_X^{(k)}(0) &= i^k \int_\mathbb{R}X^k\cdot f(X)\cdot dX \\
&= i^kE[X^k]
\end{align*}

\end{preuve}


\paragraph{}
On en déduit la formule de \emph{Mac-Laurin} : 

\begin{equation}
\varphi_X(t) = \sum_{k = 0}^{+\infty}\frac{t^k}{k!}\cdot i^k\cdot E[X^k]
\end{equation}


\begin{remarque}
La fonction caractéristique d'une somme de variables aléatoires indépendantes
est égale au produit de leurs fonctions caractéristiques.
\begin{align*}
\varphi_{X_1+X_2}(t) &= E[e^{it(X_1 + X_2)}] \\
&= E[e^{itX_1}e^{itX_2}] \\
&= E[e^{itX_1}]\cdot E[e^{itX_2}]  && \text{On peut séparer car $X_1$ et $X_2$ sont indépendantes} \\
&= \varphi_{X_1}(t)\cdot\varphi_{X_2}(t)
\end{align*}

On peut généraliser cette formule :
\begin{equation}
\varphi_{\sum_{i = 1}^{n} X_i}(t) = \prod_{i = 1}^{n} \varphi_{X_i}(t)
\end{equation}

\end{remarque}

