\section{Convergence en probabilité}

\begin{definition}
Soit $X_n$ une suite de variables aléatoires.
On dit que la suite $X_n$ converge en probabilité vers
la constante $a$ si et seulement si $\forall~\varepsilon > 0$
et $\eta > 0$, $\exists~n_0 \in \mathbb{N}^*$ tel que
$\forall n > n_0$ :

\begin{equation*}
\P(|X_n - a| > \varepsilon) < \eta \quad \Leftrightarrow \quad \lim_{n \to \infty} P(|X_n - a| > \varepsilon) = 0
\end{equation*}

Cette définition est équivalente à:

\begin{equation*}
\lim_{n \to \infty} P(|X_n - a| \leq \varepsilon) = 1
\end{equation*}
\end{definition}

\begin{notation}
Si une suite $X_n$ \emph{converge en probabilité} vers $a$ lorsque $n \rightarrow +\infty$ on note :
$$\CV{X_n}{P}{n \rightarrow +\infty}{a}$$
\end{notation}


On définit alors la convergence en probabilité vers une variable aléatoire
$X$ comme la convergence vers $0$ de la suite $X_n - X$. C'est-à-dire $\forall \varepsilon > 0$
et $\eta > 0$, $\exists~n_0 \in \mathbb{N}^*$ tel que $\forall n > n_0$ :

\begin{equation*}
P(|X_n - x| > \varepsilon) < \eta \quad \Leftrightarrow \quad \lim_{n \to +\infty} P (|X_n - X| > \varepsilon) = 0
\end{equation*}

\begin{notation}
Si une suite $X_n$ \emph{converge en probabilité} vers variable aléatoire $X$ lorsque $n \rightarrow +\infty$ on note :
$$\CV{X_n}{P}{n \rightarrow +\infty}{X}$$
\end{notation}


\subsection*{Inégalité de Chebyshev}
\label{subsec:Chebyshev}
\begin{equation*}
\boxed{\forall \varepsilon > 0\text{,}\quad P(|X - E(X)| > \varepsilon) < \frac{V(X)}{\varepsilon^2}}
\end{equation*}

Lorsque $\left\{ 
\begin{array}{l}
  \CV{E(X_n)}{}{n \rightarrow +\infty}{a} \\
  \CV{V(X_n)}{}{n \rightarrow +\infty}{0}
 \end{array} \right.
$, alors $\CV{X_n}{P}{n \rightarrow +\infty}{a}$, car
$\forall \varepsilon > 0$,

\begin{align*}
&P(|X_n - \underbrace{E(X_n)}_{\CV{}{}{n \to +\infty}{a}}| > \varepsilon)
< \underbrace{\frac{V(X_n)}{\varepsilon^2}}_{\CV{}{}{n \to +\infty}{0}} \\
&\Rightarrow \CV{P(|X_n - a| > \varepsilon)}{}{n \to +\infty}{0} \\
&\Rightarrow \boxed{\CV{X_n}{P}{n \to +\infty}{a}}
\end{align*}






\section{Convergence en moyenne d'ordre $p$}

\paragraph{}
On suppose que $E(|X_n - X|^p)$ existe, $p \in  \mathbb{N}^*$.

\begin{definition}
On dit que la suite $X_n$ converge en moyenne d'ordre $p$ vers $X$ si et
seulement si $\CV{E(|X_n - X|^p)}{}{n \to +\infty}{0}$.
\end{definition}

\begin{remarque}
La plus utilisée est la convergence en moyenne quadratique : $p = 2$.
\begin{equation*}
\CV{E(|X_n - X|^2)}{}{n \to +\infty}{0}
\end{equation*}
\end{remarque}

\begin{notation}
Si une suite $X_n$ \emph{converge en moyenne d'ordre quadratique} vers une variable aléatoire
$X$ lorsque $n \rightarrow +\infty$ on note :
$$\CV{X_n}{\text{m.q.}}{n \rightarrow +\infty}{X}$$
\end{notation}






\section{Convergence en loi}

\paragraph{}
Elle permet d'approximer la fonction de répartition de $X_n$ par celle de
$X$.

\begin{definition}
La suite $X_n$ converge en loi vers la variable aléatoire $X$, si \emph{en tout
point de continuité} de $F(x)$ (fonction de répartition de $X$), la suite
$F_n(x)$ (fonction de répartition de la suite $X_n$) converge vers $F(x)$ :

\begin{equation*}
\lim_{n \to +\infty} F_n(x) = F(x) \text{,}\quad \forall X~\text{point de continuité de
$F(x)$}
\end{equation*}
\paragraph{Rappel} $ F(x) = P[X < x]$ est la fonction de répartition de $X$.
\end{definition}

\begin{notation}
Si une suite $X_n$ \emph{converge en loi} vers une variable aléatoire
$X$ lorsque $n \rightarrow +\infty$ on note :
$$\CV{X_n}{\mathscr{L}}{n \rightarrow +\infty}{X}$$
\end{notation}


\begin{remarque}
Cette définition est équivalente à la convergence des fonctions
caractéristiques :

\begin{equation*}
\lim_{n \to +\infty} \underbrace{\varphi_{X_n}(t)}_{\substack{\text{fonction}
\\ \text{caractéristique} \\ \text{de }X_n}} =
\underbrace{\varphi_{X}(t)}_{\substack{\text{fonction}
\\ \text{caractéristique} \\ \text{de }X}}
\end{equation*}
\end{remarque}


