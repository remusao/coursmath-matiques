
\section{Convergence en loi de la binomiale vers la loi de Laplace-Gauss}

\begin{theoreme}(\emph{Moivre-Laplace})
\paragraph{}
$X_n$ étant une suite de variables binomiales $\mathscr{B}(n, p)$
alors,
\begin{equation*}
\CV{\frac{X_n - np}{\sqrt{npq}}}{\mathscr{L}}{n \to +\infty}{L.G(0, 1)} \quad\text{avec $q = 1 - p$}
\end{equation*}
\end{theoreme}

\begin{preuve}

La fonction caractéristique de la loi $\mathscr{B}(n, p)$ :
\begin{equation*}
\varphi_{X_n}(t) = (pe^{it} + 1 - p)^n \quad\text{voir exercice 1}
\end{equation*}

or,
\begin{equation*}
\varphi_{\frac{X - m}{\sigma}} = e^{-\frac{itm}{\sigma}}\cdot\varphi_X \left(\frac{t}{\sigma}\right)
\end{equation*}

donc la fonction caractéristique de $Y_n = \frac{X_n - np}{\sqrt{npq}}$ est :
\begin{equation*}
\varphi_{Y_n}(t) = (pe^{\frac{it}{\sqrt{npq}}} + 1 - p)^n\cdot e^{-\frac{itnp}{\sqrt{npq}}}
~\text{, avec} \quad
\left\{ 
\begin{array}{l}
  m = np \\
  \sigma = \sqrt{npq}
 \end{array} \right.
\end{equation*}

on applique un logarithme népérien des deux côtés de l'égalité :

\begin{equation*}
\ln\left(\varphi_{Y_n}(t)\right) = n\cdot \ln\left(p\left(e^{\frac{it}{\sqrt{npq}}} - 1\right) + 1\right)
- \frac{itnp}{\sqrt{npq}}
\end{equation*}

\begin{rappel}
développement limité à l'ordre 2 lorsque $x$ est au voisinage de $0$ :
\begin{align*}
e^x &\simeq 1 + \frac{x}{1!} + \frac{x^2}{2!} \\
\ln(1 + x) &\simeq x - \frac{x^2}{2}
\end{align*}
\end{rappel}

\paragraph{}
On applique un double développement limité :
\begin{align*}
\ln\left(\varphi_{Y_n}(t)\right) &\simeq
n\cdot \ln\left(1 + p\left(\frac{it}{\sqrt{npq}}
- \frac{t^2}{2npq}\right)\right) - \frac{itnp}{\sqrt{npq}}      \\
&\simeq n\left(\frac{pit}{\sqrt{npq}} - \frac{\cancel{p}t^2}{2n\cancel{p}q} +
\frac{p^2t^{\cancel{2}}}{2n\cancel{p}q}\right) - \frac{itnp}{\sqrt{npq}}      \\
&\simeq \cancel{\frac{npit}{\sqrt{npq}}} - \frac{t^2}{2q} +
\frac{pt^2}{2q} - \cancel{\frac{itnp}{\sqrt{npq}}}           \\
&\simeq \frac{t^2}{2q}\cdot\underbrace{(p - 1)}_{-q}                            \\
&\simeq -\frac{\cancel{q}t^2}{2\cancel{q}}                               \\
\ln\left(\varphi_{Y_n}(t)\right) &\simeq -\frac{t^2}{2}
\end{align*}

On applique une exponentielle des deux côtés de l'équation :
\begin{equation*}
\varphi_{Y_n} \simeq e^{-\frac{t^2}{2}},\quad\text{fonction caractéristique de $L.G(0, 1)$}
\end{equation*}


\paragraph{}
Au final on a bien :
\begin{equation*}
\boxed{\CV{\frac{X_n - np}{\sqrt{npq}}}{\mathscr{L}}{n \to +\infty}{L.G(0, 1)}}
\end{equation*}
\end{preuve}


\begin{remarque}
Lorsque $n$ est suffisamment grand, on peut approximer la loi $\mathscr{B}(n, p)$
par la loi de Gauss. Cependant, il convient d'effectuer ce que l'on appelle
la \emph{correction de continuité} :

\begin{align*}
P[X = x] &\simeq P\biggl[\frac{x - np - \frac{1}{2}}{\sqrt{npq}} < U
< \frac{x - np + \frac{1}{2}}{\sqrt{npq}}\biggl] \\
P[X \leq x] &\simeq P\biggl[U < \frac{x - np + \frac{1}{2}}{\sqrt{npq}}\biggl]
\end{align*}

avec, $U = \frac{X - n}{\sqrt{npq}} \simeq L.G(0.1)$.

\end{remarque}
   

\subsection*{Exemple}

\paragraph{}
Soit $X$ une variable aléatoire suivant une loi $\mathscr{B}(n, p)$ avec $\left\{ 
\begin{array}{l}
  n = 40 \\
  p = 0.3
 \end{array} \right.$
 
on a donc $\left\{ 
\begin{array}{l}
  np = 12 \\
  npq = 8,4
 \end{array} \right.$

\paragraph{}
\begin{itemize}
    \item $P[X = 11]$ :
    \begin{itemize}
        \item valeur exacte : $P[X = 11] = 0.1319$ ;
        \item formule d'approximation :
        \begin{align*}
        P[X = 11] &\simeq P\biggl[\frac{10.5 - 12}{\sqrt{8.4}}
        < U < \frac{11.5 - 12}{\sqrt{8.4}}\biggl] \\
        &\simeq P[-0.52 < U < -0.17] \\
        &\simeq 0.131
        \end{align*}
        Soit une erreur de moins de $1\%$.
    \end{itemize}
    \bigskip
    \item $P[X \leq 11]$ :
    \begin{itemize}
        \item valeur exacte : $P[X \leq 11] = 0.4406$ ;
        \item formule d'approximation :
        \begin{align*}
        P[X \leq 11] &\simeq P\biggl[U \leq
        \frac{11.5 - 12}{\sqrt{8.4}}\biggl] \\
        &\simeq 0.4325
        \end{align*}
        \item valeur sans correction de continuité :
        \begin{align*}
        P[X \leq 11] &\simeq P\biggl[U \leq \frac{-1}{\sqrt{8.4}}\biggl] \\
        &\simeq 0.3632
        \end{align*}
        Résultat très imprécis.
    \end{itemize}
\end{itemize}



\section{Convergence en loi de la loi de Poisson vers Gauss}

\begin{theoreme}
Soit $X_n$ une famille de variables de Poisson $\mathcal{P}(\lambda)$ alors
si $\lambda \to +\infty$ :
\begin{equation*}
\CV{\frac{X_\lambda - \lambda}{\sqrt{\lambda}}}{\mathscr{L}}{\lambda \to +\infty}{L.G(0, 1)}
\end{equation*}
\end{theoreme}


\begin{preuve}


La fonction de caractéristique de la variable de $\mathcal{P}(\lambda)$ est :
\begin{equation*}
\varphi_{X_\lambda}(t) = e^{-\lambda}e^{\lambda e^{it}}
= e^{\lambda e^{it} - \lambda}
\end{equation*}


La fonction caractéristique de $Y_\lambda = \frac{X_\lambda - \lambda}{\sqrt{\lambda}}$ est :
\begin{align*}
\varphi_{Y_\lambda}(t) &= e^{\lambda(e^{\frac{it}{\sqrt\lambda}} - 1)}
 \cdot e^{\frac{-it\lambda}{\sqrt{\lambda}}} \\
&= e^{\lambda e^{\frac{it}{\sqrt{\lambda}}} - \lambda -
    it\sqrt{\lambda}} \\
&\simeq e^{\lambda(1 + \frac{it}{\sqrt{\lambda}} -
    \frac{t^2}{2\lambda}) - \lambda - it\sqrt{\lambda}}  && \text{développement limité}\\
&\simeq e^{\frac{-t^2}{2}} && \text{fonction caractéristique de $L.G(0, 1)$}
\end{align*}
\end{preuve}




\section{Théorème central-limite}
\begin{theoreme}
    Soit $(X_n)$ une suite de variables aléatoires \emph{indépendantes} et
    \emph{de même loi}, d'espérance $\mu$ et d'écart type $\sigma$, alors,
    \begin{equation*}
    \CV{\frac{X_1 + X_2 + \cdots + X_n - n\mu}{\sigma{\sqrt{n}}}}
    {Loi}{n \to +\infty}{L.G(0,1)}
    \end{equation*} 
\end{theoreme}

\begin{preuve}
    \begin{equation*}
    \frac{X_1 + X_2 + \ldots + X_n + n \mu}{\sigma\sqrt{n}} =
    \sum^n_{i = 1}\left(\frac{X_i - \mu}{\sigma\sqrt{n}}\right) = S_n
    \end{equation*}
    
    et,

    \begin{equation*}
    \varphi_{S_n}(t) = \prod^n_{i = 1}\varphi_{\frac{X_i - \mu}{\sigma\sqrt{n}}}(t)
    \quad \text{(car les $X_i$ sont indépendantes.)}
    \end{equation*}
    
    
    \begin{rappel}
        Formule de Maclaurin :
        \begin{equation*}
            \varphi_{\frac{X_j - \mu}{\sigma\sqrt{n}}}(t) = \sum^{+\infty}_{k = 0}
            \frac{t^k}{k!}i^kE\left(\left(\frac{X_j - \mu}
            {\sigma\sqrt{n}}\right)^k\right)
        \end{equation*}
    \end{rappel}

    Or,
    \begin{align*}
    E\left(\frac{X_i - \mu}{\sigma\sqrt{n}}\right) &=
    \frac{1}{\sigma\sqrt{n}}\cdot E\left(X_i - \mu\right) \\
    &= \frac{E(X_i) - \mu}{\sigma\sqrt{n}} \\
    &= 0 \\
    V\left(\frac{X_i - \mu}{\sigma\sqrt{n}}\right) &=
    E\left(\left(\frac{X_i - \mu}{\sigma\sqrt{n}}\right)^2\right)
    - \cancel{E^2\left(\frac{X_i - \mu}{\sigma\sqrt{n}}\right)} \\
    &= \frac{1}{\sigma^2n}\cdot V\left(X_i - \mu\right) \\
    &= \frac{V(X_i)}{\sigma^2n} \\
    &= \frac{\cancel{\sigma^2}}{\cancel{\sigma^2}n} \\
    &= \frac{1}{n}
    \end{align*}

    Maclaurin à l'ordre 2 nous donne :

    \begin{equation*}
    \varphi_{\frac{X_i - \mu}{\sigma\sqrt{n}}}(t) \approx 1
    - \frac{t^2}{2n}
    \end{equation*}        

    donc,
    
    \begin{align*}
    \varphi_{S_n}(t) &\approx \prod^n_{i = 1}\left(1 - \frac{t^2}{2n}\right) \\
    &\approx \left(1 - \frac{t^2}{2n}\right)^n
    \end{align*}
    
    \begin{rappel}
        \begin{equation*}
        \CV{\left(1 + \frac{X}{n}\right)^n}{}{n \to +\infty}{e^X}
        \end{equation*}
    \end{rappel}

    donc,
    \begin{equation*}
    \lim_{n\rightarrow +\infty}\varphi_{S_n}(t) = e^{-\frac{t^2}{2}}
    \end{equation*}
    qui est la fonction caractéristique de $L.G(0,1)$.
    Conclusion :
    \begin{equation*}
    \CV{\frac{X_1 + X_2 + \cdots + X_n - n\mu}{\sigma{\sqrt{n}}}}
    {Loi}{n \to +\infty}{L.G(0,1)}
    \end{equation*}
\end{preuve}





\section{Exercice 1}

\paragraph{\'Enoncé}
Soit la suite $X_n$ de variables aléatoires dont la densité est :
\begin{equation*}
f_n(x) = \frac{ne^{-nx}}{(1 + e^{-nx})^2}
\end{equation*}

\paragraph{Question}
Montrer que $\CV{X_n}{P}{n \to +\infty}{0}$.

\paragraph{Solution}
On chercher à savoir si $\forall \varepsilon > 0$, $\CV{P[|X_n| > \varepsilon]}{}{n \to +\infty}{0}$.\\
On a $P[|X_n| > \varepsilon] = 1 - P[|X_n| \leq \varepsilon]$.

\begin{rappel}
\begin{equation*}
P[a < X < b] = \int_a^b f(x)dx = F(b) - F(a)
\end{equation*}
\end{rappel}

\paragraph{}
Ici on a :
\begin{align*}
P[|X_n| > \varepsilon] &= 1 - \int_{-\varepsilon}^\varepsilon f_n(x)dx \\
&= 1 - \int_{-\varepsilon}^\varepsilon \frac{n e^{-nx}}{(1 + e^{-nx})^2}dx \\
&= 1 - \biggl[\frac{1}{1 + e^{-nx}}\biggl]_{-\varepsilon}^{\varepsilon} \\
&= 1 - \underbrace{\frac{1}{1 + e^{-n\varepsilon}}}_{\CV{}{}{n \to +\infty}{1}}
+ \underbrace{\frac{1}{1 + e^{n\varepsilon}}}_{\CV{}{}{n \to +\infty}{0}}
\end{align*}

Donc :
\begin{equation*}
\CV{P[|X_n| > \varepsilon]}{}{n \to +\infty}{0} \quad\Leftrightarrow\quad \boxed{\CV{X_n}{P}{n \to +\infty}{0}}
\end{equation*}



\section{Exercice 2}
\paragraph{\'Enoncé}
Soit $X$ une variable aléatoire continue de densité :
\begin{equation*}
g(x) = e^{-x - e^{-x}}
\end{equation*}


\paragraph{Questions}

\begin{enumerate}
    \item Déterminer la fonction de répartition $G$ de $X$.
    \item Déterminer la fonction de répartition $H$ de $Z = e^{-X}$, ainsi que sa densité.
    \item Calculer $E(Z)$ et $V(Z)$.
    \item Soit $(Z_1, \ldots, Z_n)$ un échantillon de $Z$ et $\overline{Z}_n = \frac{1}{n}\sum_{i = 1}^n Z_i$ :
    \begin{enumerate}
        \item montrer que $\CV{\overline{Z}_n}{P}{n \to +\infty}{1}$ ;
        \item montrer que $\CV{\overline{Z}_n}{m.q}{n \to +\infty}{1}$.
    \end{enumerate}
\end{enumerate}

\paragraph{Solution}

\begin{enumerate}
    \item % Question 1
    \begin{align*}
    G(x) &= P[X < x] \\
    &= \int_{-\infty}^x g(t)dt \\
    &= \int_{-\infty}^x e^{-t - e^{-t}}dt \\
    &= \biggl[-e^{-e^{-t}}\biggl]_{-\infty}^x \\
    G(x) &= \boxed{e^{-e^{-x}}}
    \end{align*}
    \bigskip
    
    \item % Question 2
    Fonction de répartition de $Z = e^{-X}$ :
    \begin{align*}
    H(z) &= P[Z < z] && \forall z > 0 \\
    &= P[ e^{-X}< \ln(z)] \\
    &= P[-X < \ln(z)] \\
    &= P[X > -\ln(z)] \\
    &= 1 - G(-\ln(z))
    \end{align*}
    Or on connait $G(x)$, donc :
    \begin{equation*}
    \boxed{H(z) = 1 - e^{-z}, \quad \forall z > 0}
    \end{equation*}
    Enfin, sa densité $h(z)$ est :
    \begin{equation*}
    \boxed{h(z) = H'(z) = e^{-z}, \quad \forall z > 0}
    \end{equation*}
    \bigskip
    
    \item % Question 3
    L'espérance de $Z$ est :
    \begin{align*}
    E(Z) &= \int_0^{+\infty} h(z)dz \\
    &= \biggl[-ze^{-z}\biggl]_0^{+\infty} + \int_0^{+\infty} e^{-z}dz \\
    &= -\biggl[e^{-z}\biggl]_0^{+\infty} \\
    &= 1
    \end{align*}
    
    \begin{equation*}
    \boxed{E(Z) = 1}
    \end{equation*}
    
    La variance de $Z$ est :
    
    \begin{equation*}
    V(Z) = E(Z^2) - E^2(Z)
    \end{equation*}

	avec,    
    
    \begin{align*}
    E(z^2) &= \int_0^{+\infty}z^2h(z)dz \\
    &= \int_0^{+\infty}z^2e^{-z}dz \\
    &= [-z^2e^{-z}]_0^{+\infty} + \int_0^{+\infty}2ze^{-z}dz
    && \text{par partie} \\
    &= 2\cdot\int_0^{+\infty}ze{-z}dz \\
    &= 2\cdot E(z) \\
    E(z^2) &= 2
    \end{align*}
    
    donc,
    
    \begin{equation*}
    \boxed{V(Z) = 1}
    \end{equation*}
	
	\item % Question 4


	Soit $(Z_1, \ldots, Z_n)$ un échantillon de $Z$, c'est-à-dire que
	$Z_i$ est une suite de variables aléatoires indépendantes de
	même loi que $Z$.
	\begin{equation*}
	\overline{Z}_n = \frac{1}{n}\sum_{i = 1}^nZ_i \quad \text{moyenne empirique}
	\end{equation*}

	\begin{enumerate}
		\item % a)
		
		On cherche à montrer que :
		$\CV{\overline{Z_n}}{P}{n \to +\infty}{1} = E(Z)$. Pour cela,
		on utilise l'inégalité de Chebyshev (\ref{subsec:Chebyshev}) :

		\begin{align*}
		E(\overline{Z}_n) &= \frac{1}{n}\sum_{i=1}^nE(Z_i) \\
		&= \frac{1}{n}\sum_{i=1}^n1 \\
		&= \frac{\cancel{n}}{\cancel{n}} \\
		&= 1
		\end{align*}
		
		de même,
		
		\begin{align*}
		V(\overline{Z}_n) &= \frac{1}{n^2}\cdot V\left(\sum_{i=1}^nZ_i\right) \\
		&= \frac{1}{n^2}\cdot\sum_{i=1}^nV(Z_i)  && \text{car les variables sont
		indépendantes}\\
		&= \frac{1}{n^2}\cdot\sum_{i=1}^n1  \\
		&= \frac{\cancel{n}}{n^{\cancel{2}}}  \\
		&= \frac{1}{n}
		\end{align*}
		
		donc,
		
		\begin{equation*}
		V(\overline{Z}_n) = \CV{\frac{1}{n}}{}{n \to +\infty}{0}		
		\end{equation*}
		
		et $\forall \varepsilon > 0$, 

		\begin{align*}
		&P(|\overline{Z}_n - 1| \ge \varepsilon) <
		\underbrace{\frac{1}{n\varepsilon^2}}_{\CV{}{}{n \to +\infty}{0}} \\
		&\Rightarrow \lim_{n \to +\infty} P(|\overline{Z_n} - 1| >
		\varepsilon ) = 0 \\
		&\Rightarrow \boxed{\CV{\overline{Z}_n}{P}{n \to +\infty}{1} = E(Z)}
		\end{align*}
		
		\bigskip
		\item % b)
		On cherche à montrer que :
		$\CV{\overline{Z}_n}{m.q}{n \to +\infty}{1} = E(Z)$, donc que \\
		$\CV{E(|\overline{Z}_n - 1|^2)}{}{n \to + \infty}{0}$.
		
		\paragraph{}
		On remarque que :
		\begin{equation*}
		E(|\overline{Z}_n - 1|^2) = E(|\overline{Z}_n - E(\overline{Z}_n)|^2)
		= V(\overline{Z}_n)
		\end{equation*}

		\begin{rappel}
		Par définition :
		$$V(X) = E((X - E(X))^2))$$
		Mais en pratique on utilise plus souvent la formule König-Huyghens :
		$$V(X) = E(X^2) - E(X)^2$$
		\end{rappel}

		On conclue immédiatement que,
		
		\begin{align*}
		E(|\overline{Z}_n - E(\overline{Z}_n)|^2) &= V(\overline{Z}_n)  \\
		&= \CV{\frac{1}{n}}{}{n \to +\infty}{0} \\
		&\Rightarrow \boxed{\CV{\overline{Z_n}}{m.q}{n	\to +\infty}{1}}
		\end{align*}
	\end{enumerate}
\end{enumerate}



\section{Exercice 3}

\paragraph{\'Enoncé}
Soit $X$ une variable aléatoire de loi gamma $\gamma_p$

\paragraph{Questions}
\begin{enumerate}
	\item Calculer la fonction caractéristique de $X$ ;
	\item en déduire celle de $Y_p = \frac{X - p}{\sqrt{p}}$ ;
	\item montrer que $\CV{\frac{X - p}{\sqrt{p}}}{\mathscr{L}}{p \to
+\infty}L.G(0,1)$.
\end{enumerate}


\paragraph{Solution}

\begin{rappel}
On rappelle la densité de la loi $\gamma_p$ :
	\begin{equation*}
	f(X) = \frac{1}{\Gamma(p)}\cdot X^{p-1} \cdot e^{-X} \quad \forall X > 0
	\end{equation*}
\end{rappel}


\begin{enumerate}
	\item % Solution Question 1
	
	La fonction caractéristique de $X$ est :
	
	\begin{align*}
	\varphi_X(t) &= \int_{\mathbb{R}}e^{itx}f(x)dx \\
	&= \int_0^{+\infty}e^{itx}\frac{1}{\Gamma(p)}\cdot x^{p-1}\cdot e^{-x}dx \\
	&= \frac{1}{\Gamma(p)}\cdot\int_0^{+\infty}e^{(it - 1)x}x^{p-1}dx
	\end{align*}


	Posons : $I_{p-1} = \int_0^{+\infty}e^{(it - 1)x}x^{p-1}dx$
	
	\paragraph{}
	Calculons les premiers termes de la suite pour essayer de faire
	apparaitre une relation de récurrence :
	
	\begin{align*}
	I_0 &= \int_0^{+\infty} e^{(it - 1)x}dx \\
	&= \left[\frac{e^{(it - 1)x}}{it - 1}\right]_0^{+\infty}
	\end{align*}

    or,
    
    \begin{equation*}
    e^{(it - 1)x} = \CV{e^{itx}e^{-x}}{}{x \to \infty}{0}
    \end{equation*}
	car $|e^{itx}| = 1$.

    \begin{equation*}
    \boxed{I_0 = \frac{-1}{it - 1}}
    \end{equation*}

    Ensuite, en intégrant par partie avec $\left\{
    \begin{array}{l}
      \CV{v=x^{p - 1}}{}{}{v' = (p - 1)x^{p-2}} \\
      \CV{u'= e^{(it - 1)x}}{}{}{u = \frac{e^{(it - 1)x}}{it - 1}} \\
    \end{array} \right.$, on obtient :
    
    \begin{align*}
    I_{p - 1} &= \int_0^{+\infty}e^{(it - 1)x}x^{p - 1}dx \\
    &= \underbrace{\cancel{\left[\frac{e^{(it - 1)x}}{it - 1}
    x^{p - 1}\right]_0^{+\infty}}}_{\CV{}{}{x \to +\infty}{0}}
    - \frac{p - 1}{it - 1}\int_0^{+\infty}e^{(it - 1)x}x^{p - 1}dx \\    
    &= -\frac{p - 1}{it - 1}\cdot I_{p - 2} \quad\forall p \ge 2
    \end{align*}
 
    Nous avons donc notre relation de récurrence :
    \begin{equation*}
    \boxed{\left\{
    \begin{array}{l}
        I_0 = \frac{-1}{it - 1} \\
        I_{p - 1}= -\frac{p - 1}{it - 1}\cdot I_{p - 2} \\
    \end{array} \right.}
    \end{equation*}        

    Ainsi par récurrence on obtient :
    
    \begin{align*}
    I_{p - 1} &= -\frac{(p - 1)}{it - 1}\cdot\cancel{I_{p-2}} \\
    \cancel{I_{p - 2}} &= -\frac{(p - 2)}{it - 1}\cdot\cancel{I_{p-3}} \\
    \cancel{I_{p - 3}} &= -\frac{(p - 4)}{it - 1}\cdot\cancel{I_{p-4}} \\
    &\ldots \\
    \cancel{I_{2}} &= -\frac{(2)}{it - 1}\cdot\cancel{I_{1}} \\
    \cancel{I_{1}} &= -\frac{(1)}{it - 1}\cdot I_0
    \end{align*}

    En multipliant membre à membre toutes les équations, on obtient :
    \begin{equation*}
    \boxed{I_{p-1} = \frac{(-1)^{p-1}(p - 1)!\cdot I_0}{(it - 1)^{p-1}}
    = \frac{(-1)^p(p - 1)!}{(it - 1)^{p}} \quad \forall p \ge 1}
    \end{equation*}


    Finalement,
    
    \begin{align*}
    \varphi_X(t) &= \frac{1}{\Gamma(p)}\cdot I_{p - 1} \\
    &= \frac{1}{\cancel{(p-1)!}}\cdot\frac{(-1)^p\cancel{(p-1)}!}{(it - 1)^p} \\
    &= (1 - it)^{-p}
    \end{align*}

    \begin{equation*}
    \boxed{\varphi_X(t) = (1 - it)^{-p}}
    \end{equation*}




    \item % Solution Question 2
    On cherche la fonction caractéristique de
    $Y_p = \frac{X - p}{\sqrt{p}}$ (centrée réduite).
    
    \begin{rappel}
    \begin{equation*}
        \varphi_{\frac{X - m}{\sigma}}(t) = e^{-\frac{itm}{\sigma}}\cdot
        \varphi_X\left(\frac{t}{\sigma}\right)
    \end{equation*}
    \end{rappel}
    
    \begin{equation*}
    \varphi_{\frac{X - p}{\sqrt{p}}}(t) = e^{-\frac{itp}{\sqrt{p}}}
    (1 - \frac{it}{\sqrt{p}})^{-p}
    \end{equation*}
    
    \begin{equation*}
    \varphi_{Y_p}(t) = e^{-it\sqrt{p}}(1 - \frac{it}{\sqrt{p}})^{-p}
    \end{equation*}

    \bigskip
    \item % Solution Question 3
     On veut montrer qu'il s'agit d'une gaussienne. On applique un logarithme
     népérien sur chacun des membres de l'équation :
     
     \begin{align*}
     \ln\left({\varphi_{Y_p}(t)}\right) &=
     \ln\left(e^{-it\sqrt{p}}(1 - \frac{it}{\sqrt{p}})^{-p}\right) \\
     &= \ln\left(e^{-it\sqrt{p}}\right)
     + \ln\left(\left(1 - \frac{it}{\sqrt{p}}\right)^{-p}\right) \\
     &= -it\sqrt{p} - p\ln\left(1 - \frac{it}{\sqrt{p}}\right)
     \end{align*}

    On va effectuer un développement limité du logarithme népérien à l'ordre
    2 afin de simplifier l'expression, puis appliquer une exponentielle
    sur les deux membres de l'équation afin de retrouver $\varphi_{Y_p}(t)$.

    \begin{rappel} 
        $\ln(1 + x) \simeq x - \frac{x^2}{2}\quad$ à l'ordre 2.
    \end{rappel}

    Ainsi,
    \begin{align*}
    \ln \varphi_{Y_p}(t) &\simeq -it\sqrt{p}
    - p(-\frac{it}{\sqrt{p}} + \frac{t^2}{2p}) \\
    &\simeq \cancel{-it\sqrt{p}} + \cancel{\frac{itp}{\sqrt{p}}} - \frac{t^2}{2}
    \end{align*}

    Enfin, en repassant à l'exponentielle on obtient,
    
    \begin{equation*}
    \varphi_{Y_p}(t) = e^{-\frac{t^2}{2}} \quad
    \text{fonction caractéristique de $L.G(0, 1)$}
    \end{equation*}
    
    Conclusion,
    \begin{equation*}
    \boxed{\CV{Y_p}{\mathscr{L}}{p \to +\infty}{L.G(0, 1)}}
    \end{equation*}
\end{enumerate}




\section{Exercice 4}

\paragraph{\'Enoncé}
Le nombre de pannes par mois, sur une certaine machine suit une loi de
Poisson $\mathcal{P}(3)$. Un atelier fonctionne avec 12 machines de ce type.
Les machines sont indépendantes.

\paragraph{Questions}
\begin{enumerate}
    \item En un mois, quelle est la probabilité de constater plus de 42
    pannes dans cet atelier ?
    \item En un mois, quelle est la probabilité de constater entre 36
    et 45 pannes dans cet atelier ?
\end{enumerate}


\paragraph{Solution}
\begin{enumerate}

    \item %Question 1
    Soit $X_i$ la variable aléatoire qui représente le nombre de pannes
    en un mois de la machine $i$ ($1 \le i \le 12$). $X_i$ suit une loi
    de Poisson $\mathcal{P}(\lambda = 3)$.

    Soit $S_n = \sum_{i = 1}^nX_i$, avec $X_i$ variables indépendantes
    (dans notre cas $n = 12$).


    % Rappel
    \begin{rappel}
    Soit $X_\lambda$ une suite de variables aléatoires de Poisson
    $\mathcal{P}(\lambda)$ :
    \begin{equation*}
    \CV{\frac{X_\lambda - \lambda}{\sqrt{\lambda}}}{\mathscr{L}}
    {\lambda \to +\infty}{L.G(0,1)}
    \end{equation*}

    En pratique, on obtient une bonne approximation pour $\lambda \ge 18$.
    \end{rappel}


    $S_n$ est une somme de variables aléatoires indépendantes de Poisson.
    $S_n$ suit une loi de Poisson $\mathcal{P}(n\lambda = \lambda')$.
    $$\lambda' = 12 \times 3 = 36$$
    Donc $S_n$ suite une loi de Poisson $\mathcal{P}(36)$.
    On cherche la probabilité : $$P(S_n > 42)$$
    On se propose de centrer-réduire $S_n$, puis d'utiliser le théorème
    rappelé ci-dessus afin de pouvoir utiliser la table de valeurs
    d'une loi gaussienne centrée-réduite (fournie en annexe) :
    
    \begin{align*}
    P(S_n > 42) &= 1 - P(S_n \le 42) \\
    &= 1 - P\left(\frac{S_{12} - 36}{6} \le \frac{42 - 36}{6}\right)
    \end{align*}

    Posons $U = \frac{S_{12} - 36}{6} \simeq L.G(0,1)$
    
    \begin{align*}
    P(S_n > 42) &\simeq 1 - P[U \le 1] \\
    &\simeq 1 - F(1)  \\
    &\simeq -0.1587
    \end{align*}


    \item %Question 2
    \begin{align*}
    P(36 \le S_n \le 45) &\simeq P\left(0 \le U \le \frac{45 - 36}{6}\right) \\
    &\simeq P\left(0 \le U \le \frac{3}{2}\right) \\
    &\simeq F(1.5) - F(0) \\
    &\simeq 0.4332
    \end{align*}
\end{enumerate}




\section{Exercice 5}

\paragraph{\'Enoncé}
Une usine fabrique des pièces dont 3\% ont des défauts. Soit $X$
la variable aléatoire représentant le nombre de pièces défectueuses.
$X$ suit une loi binomiale $\mathcal{B}(n, p)$ avec $p =3$ ($n$
dépendra de la question).

\paragraph{Questions}
\begin{enumerate}
  \item On prélève 1000 pièces au hasard, quelle est la probabilité
  d'avoir plus de 50 pièces défectueuses ?
  \item Calculer $P(20 \leq X \leq 40)$.
  \item On veut 1950 pièces sans défaut. On en prélève par prudence
  2000 au hasard. Quelle est la probabilité d'avoir suffisamment
  de pièces en bon état ?
\end{enumerate}


\paragraph{Solution}
\begin{enumerate}

    \item % Question 1
    ~
        \begin{rappel}
            Théorème de Moivre-Laplace :
            \begin{equation*}
                \CV{\mathcal{B}(n, p)}{Loi}{n \to +\infty}{L.G(np, \sqrt{npq})}
            \end{equation*}
        \end{rappel}
    
    \begin{align*}
    P(X > 50] &= 1 - P(X \leq 50) \\
    &\approx 1 - P\left(\frac{X - np}{\sqrt{npq}} \leq
    \frac{50 - np + 0.5}{\sqrt{npq}}\right)
    \end{align*}
    
    On rajoute $0.5$ pour effectuer une correction de continuité. On a
    $U = \frac{X - np}{\sqrt{npq}}$ qui suit une loi $L.G(0, 1)$ donc on
    peut utiliser la table de valeurs en annexe :
    
    \begin{equation*}
    \boxed{P(X > 50) \approx 1 - F(3.8) \approx 0}
    \end{equation*}
    
    \bigskip
    
    \item % Question 2
    \begin{equation*}
    P(20 \leq X \leq 40) \approx P\left(\frac{20 - np - 0.5}{\sqrt{npq}}
    \leq \frac{X - np}{\sqrt{npq}} \leq \frac{40 - np + 0.5}{\sqrt{npq}} \right)
    \end{equation*}
    
    avec $U = \frac{X - np}{\sqrt{npq}} \approx L.G(0, 1)$, donc
    \begin{align*}
    P(20 \leq X \leq 40) &\approx F(1.94) - F(-1.94) \\
    &\approx 2\cdot F(1.94) - 1 \\
    &\approx 0.9476
    \end{align*}
    
    \bigskip
    
    \item % Question 3
    Ici on a les paramètres $p = 3$ et $n = 2000$. On rappelle que $X$
    suit une loi binomiale $\mathcal{B}(n, p) \approx L.G(0, 1)$.
    
    \begin{align*}
    P(X \leq 50) &\approx P\left(\frac{X - np}{\sqrt{npq}}
    \leq \frac{50 - np + 0.5}{\sqrt{npq}} \right) \\
    &\approx P\left(U \leq \frac{50 - 60 + 0.5}{\sqrt{npq}} \right) \\
    &\approx 0.1056
    \end{align*}
\end{enumerate}
% TODO


\section{Exercice 6}

\paragraph{\'Enoncé}
Soit,
\begin{equation*}
U_n= \sum_{i = 1}^n X_i^2
\end{equation*}
Où les $X_i$ suivent une loi $L.G(0, 1)$ et sont indépendantes. $U_n$
est une variable $\chi$-2 de degré de liberté n.

\begin{equation*}
\chi_n = ||X||^2 \quad \text{avec, } X = (X_1, X_2, \cdots, X_n)
\quad \text{vecteur gaussien}
\end{equation*}

\paragraph{Questions}
Montrer que :
\begin{equation*}
\CV{\frac{U_n - n}{\sqrt{2n}}}{Loi}{n \to +\infty}{L.G(0, 1)}
\end{equation*}

\paragraph{Solution}
Il faut montrer que : $\left\{
        \begin{array}{l}
          E(U_n) = n \\
          \sigma(U_n) = \sqrt{2n}
        \end{array}
        \right.$

\begin{align*}
E(U_n) &= \sum_{i=1}^{n} E(X_i^2) \\
&= \sum{i=1}{n} 1 \\
&= n
\end{align*}

\begin{align*}
\sigma(U_n) &= \sigma\left(\sum_{i=1}^{n} X_i^2\right) \\
&= \sqrt{V\left(\sum_{i=1}^{n} X_i^2\right)} \\
&= \sqrt{\sum_{i=1}^{n} V(X_i^2)} \\
&= \sqrt{\sum_{i=1}^{n} \left(E(X_i^4) - E^2(X_i^2)\right)}
\end{align*}

\begin{rappel}
Si $X$ suit une loi gaussienne $L.G(0,1)$, alors grâce à la formule
des moments on a :
\begin{equation*}
    E(X^{2k}) = \frac{(2k)!}{2^k k!}
\end{equation*}
\end{rappel}

On obtient donc :

\begin{align*}
E(X_i^4) &= \frac{4!}{4\cdot 2!} = 3 \\
V(X_i^2) &= E(X_i^4) - E^2(X_i^2) = 3 - 1 = 2 \\
V(U_n) &= \sum_{i=1}^n 2 = 2n
\end{align*}

d'où,
\begin{equation*}
\boxed{\sigma(U_n) = \sqrt{2n}}
\end{equation*}

conclusion, grâce au \emph{théorème de central-limite} on obtient :
\begin{equation*}
\CV{\frac{U_n - n}{\sqrt{2n}}}{Loi}{n \to +\infty}{L.G(0, 1)}
\end{equation*}