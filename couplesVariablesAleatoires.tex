\section{Cas discret}

\begin{definition}
On appelle loi conjointe, la loi du couple $(X, Y)$,
où $X$ et $Y$ sont des variables aléatoires discrètes,  définie par :
\begin{equation*}
    P\left((X = x_i)\cap(Y = y_j)\right) = p_{ij}
    \quad \text{(Loi de l'intersection)}
\end{equation*}


  \begin{center}
  \begin{tabular}{c|cc|c|cc|c}
    X~\textbackslash ~Y & $y_1$ & $\cdots$ & $y_j$ & $\cdots$ & $y_q$ & \\
    \hline
    $x_1$ &     &        & \vdots &     &    & $p_{1 \cdot}$  \\
    $\vdots$ &   &  & $\vdots$ &   &  & \\
    \hline
    $x_i$ & $\cdots$  &  $\cdots$ & $p_{ij}$ &  $\cdots$  &  $\cdots$
    & $p_{i \cdot}$ \\
    \hline
    $\vdots$ &   &  & $\vdots$ &   &  \\
    $x_p$ &     &        & \vdots &     &    & $p_{p \cdot}$ \\
    \hline
    & $p_{\cdot 1}$ &         &    $p_{\cdot j}$   &     & $p_{\cdot q}$ &
  \end{tabular}
\end{center}

\begin{equation*}
\sum_{i, j} P_{ij} = 1
\end{equation*}
\end{definition}


\begin{definition}
On appelle \emph{lois marginales}, les lois de probabilité de $X$ et $Y$
prises séparément :
    \begin{description}
        \item[Loi marginale de $X$]
        \begin{equation*}
            P[X = x_i] = \sum_{j=1}^q P_{ij} = P_{i\cdot}
        \end{equation*}
        \item[Loi marginale de $Y$]
        \begin{equation*}
            P[Y = y_j] = \sum_{i=1}^q P_{ij} = P_{\cdot j}
        \end{equation*}
    \end{description}
\end{definition}

\begin{definition}
\emph{Lois conditionnelles} :
\begin{description}
    \item[Si $Y = y_j$]
    \begin{equation*}
    P(X = x_i \text{~\textbackslash~} Y = y_j) = \frac{P_{ij}}{P_{\cdot j}}
    = \frac{P\left((X = x_i) \cap (Y = y_j)\right)}{P(Y = y_j)}
    \end{equation*}
    \item[Si $X = y_i$]
    \begin{equation*}
    P(Y = y_j \text{~\textbackslash~} X = x_i) = \frac{P_{ij}}{P_{i \cdot}}
    = \frac{P\left((X = x_i) \cap (Y = y_j)\right)}{P_{i\cdot}}
    \end{equation*}
\end{description}
\end{definition}

\begin{definition}
Les variables aléatoires $X$ et $Y$ sont indépendantes si et seulement si :
    \begin{equation}
        P_{ij} = P_{i\cdot} \times P_{\cdot j}
    \end{equation}
\end{definition}

\begin{definition}
\begin{equation*}
E(X\cdot Y) = \sum_{i,j}x_iy_jP_{ij} = \sum_{i,j}x_iy_jP\left((X = x_i)\cap(Y = y_j)\right)
\end{equation*}
\end{definition}


\begin{definition}
    On note $Cov(X,Y) = E(X\cdot Y) - E(X)E(Y)$ la covariance de couple $(X,Y)$.
  On note le coefficient de corrélation linéaire entre X et Y :
  \begin{equation*}
  \rho(X,Y) = \frac{Cov(X,Y)}{\sigma_x\sigma_y} = \frac{Cov(X,Y)}
  {\sqrt{V(X)}\sqrt{V(Y)}}
  \end{equation*}
\end{definition}


\section{Cas continu}

\begin{definition}
    Soient $X$ et $Y$ deux variables aléatoires continues. La loi du
    couple $(X,Y)$ est définie par la densité :
    \begin{equation*}
        f(x,y)~:~\left\{
        \begin{array}{ll}
          (i) & f(x,y) \geq 0
          \\
          (ii) & \int\int_{\mathbb{R}^2}f(x,y)dxdy = 1
        \end{array}
        \right.
    \end{equation*}

\end{definition}


\begin{definition}
    la fonction de répartition $F(x,y)$ du couple $(X, Y)$ est définie par :
    \begin{align*}
    F(x,y) &= P\left[X < x \text{ et } Y < y\right] \\
    &= \int^x_{-\infty}\int^y_{-\infty}f(x,y)\cdot dx \cdot dy
    \end{align*}
    \begin{equation*}
    \frac{\partial^2F(x,y)}{\partial{}x\partial{}y} = f(x,y)
    \end{equation*}
  
\end{definition}


\begin{definition}
\emph{Lois marginales} :
\begin{description}
    \item[Loi marginale de $X$]
    \begin{equation*}
        g(x) = \int_{\mathbb{R}}f(x,y)dy
    \end{equation*}
    \item[Loi marginale de $Y$]
    \begin{equation*}
        h(x) = \int_{\mathbb{R}}f(x,y)dx
    \end{equation*}
\end{description}
\end{definition}


\begin{definition}
\emph{Lois conditionnelles} :
\begin{description}
    \item[Loi conditionnelle de $X$]
    \begin{equation*}
    f_{X/Y}(x~/~Y=y) = \frac{f(x,y)}{h(y)}
    \end{equation*}
    \item[Loi conditionnelle de $X$]
    \begin{equation*}
    f_{X/Y}(y~/~X=x) = \frac{f(x,y)}{g(y)}
    \end{equation*}
\end{description}
\end{definition}

\begin{definition}
\begin{equation*}
E(X\cdot Y) = \int\int_{\mathbb{R}^2}x\cdot y\cdot f(x,y)\cdot dx\cdot dy
\end{equation*}
\begin{equation*}
        Cov(X,Y) = E(X\cdot Y) - E(X)E(Y)
\end{equation*}
\begin{equation*}
        \boxed{\rho(X,Y) = \frac{Cov(X,Y)}{\partial{}x\partial{}y}} \quad
        \text{, coefficient de corrélation linéaire.}
\end{equation*}
\end{definition}

\begin{remarque}
Le produit scalaire défini sur l'espace des variables aléatoires :
\begin{align*}
\left<X,Y\right> &= E(X\cdot Y) \\
||X|| &= \sqrt{E(X^2)} \\
\sigma &= \sqrt{V(X)} \\
&= \sqrt{E\left((X - E(X))^2\right)} \\
&= || X - E(X) ||  && \text{ norme de } X - E(X) \\
Cov(X,Y) &= \left<X - E(X), Y - E(Y)\right> && \text{ produit scalaire des
variables centrées} \\
\rho(X,Y) &= \frac{Cov(X,Y)}{\sigma_x\sigma_y} \\
&= \frac{\left<X - E(X), Y - E(Y)\right>}{||X - E(X)|| ||Y - E(X)||} \\
&= \cos\left(\widehat{X - E(X), Y - E(Y)}\right)
\end{align*}
On a donc toujours :
\begin{equation*}
    \boxed{|\rho| \leq 1}
\end{equation*}
\end{remarque}