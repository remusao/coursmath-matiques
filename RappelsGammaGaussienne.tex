\section{Loi gamma}

\begin{definition}
On dit qu'une variable aléatoire positive $X$ suit une loi gamma de paramètre
$r$ notée $\gamma_r$ , si sa densité est donnée par:


\begin{equation}
f(X) = \frac{1}{\Gamma(r)} \cdot e^{-x} \cdot X^{r - 1}, \quad \forall X > 0
\end{equation}


où la fonction $\Gamma(X)$ est définie par :
\begin{equation}
\Gamma(X) = \int^{+\infty}_0e^{-t} \cdot t^{X - 1} dX, \quad \forall X > 0
\end{equation}

\end{definition}

\begin{propriete}
\begin{align}
    &\Gamma(X + 1) = x\cdot\Gamma(X)\\
    &\Gamma(1) = 1 \\
    &\Gamma(n + 1) = n! && \forall n \in \mathbb{N}^{*} \\
    &\lim_{X\to 0^+}\Gamma(X) = +\infty \\
    &\Gamma(k + \frac{1}{2}) = \frac{1\times3\times5\times\cdots\times(2k - 1)}{2^k}\cdot\Gamma(\frac{1}{2}) && \forall n \in \mathbb{N}^{*} \\
    &\Gamma(\frac{1}{2}) = \sqrt{\pi}
\end{align} 
\end{propriete}

\begin{remarque}
La fonction gamma est généralement perçue comme un prolongement de la factorielle à l'ensemble des nombres complexes
(excepté les entiers négatifs ou nuls).
\end{remarque}


\paragraph{Espérance de la loi $\gamma_r$}
\begin{align}
    E(X) &= \int_0^{+\infty} X \cdot f(x) \cdot dx \notag \\
    &= \frac{1}{\Gamma(r)}\int_0^{+\infty}X^r \cdot e^{-x} \cdot dx \notag \\
    &= \frac{\Gamma(r + 1)}{\Gamma(r)} \notag \\
    E(X) &= r
\end{align}

\paragraph{Variance de la loi $\gamma_r$}

\begin{equation*}
V(X) = E(X^2) - E^2(X)
\end{equation*}

Calculons $E(X^2)$ :
\begin{align*}
E(X^2) &= \int_0^{+\infty} \cdot X^2 \cdot f(X) \cdot dX   \\
&= \frac{1}{\Gamma(r)} \cdot \int_0^{+\infty} X^{r + 1}e^{-X}dX    \\
&= \frac{\Gamma(r + 2)}{\Gamma(r)}                           \\
&= \frac{\Gamma(r + 2)}{\Gamma(r + 1)} \cdot \frac{\Gamma(r + 1)}{\Gamma(r)} \\
&= (r + 1) \cdot r
\end{align*}

On obtient donc :
\begin{equation}
V(X) = r
\end{equation}





\section{Loi de Laplace-Gauss (loi normale)}

\begin{definition}
On dit qu'une variable aléatoire $X$ suit une loi gaussienne Laplace--Gauss de paramètres ($m, \sigma$),
si sa densité est :
\begin{equation}
f(X) = \frac{1}{\sigma \cdot \sqrt{2\cdot\pi}} \cdot e^{-\frac{1}{2}\cdot(\frac{X - m}{\sigma})^2}
\end{equation}

avec,
\begin{align*}
m &= E(X) && \text{(espérance)} \\
\sigma &= \sqrt{V(x)} && \text{(écart-type)}
\end{align*}
\end{definition}

Soit $U = \frac{X - m}{\sigma}$ une variable normale centrée et réduite.
On peut dire que $U$  suit une loi $L.G~(0, 1)$ ($m = 0$ et $\sigma = 1$).
On dit qu'elle est centrée car la courbe de la Gaussienne est centrée sur
l'axe des abscisse et réduite parce $f(X)$ est compris entre $0$ et $1$. De
cette manière, on pourra utiliser une table (voir Annexe) contenant les valeurs de la gaussienne,
pour toutes les variables aléatoires suivant une loi $L.G$ à condition de la réduire
et de la centrer.

\paragraph{}
La fonction de répartition est la suivante :

\begin{equation*}
F(u) = P[U < u] = \int_\mathbb{R} f(t) \cdot dt
\end{equation*}

et sa densité est :

\begin{equation*}
f(u) = \frac{1}{\sqrt{2\cdot\pi}}\cdot e^{-\frac{1}{2}\cdot u^2}
\end{equation*}

\begin{propriete}
$ V(U) = 1$ avec $U$ qui suit une loi $L.G~(0, 1)$
\end{propriete}

\begin{preuve}
\begin{align*}
    V(U) &= E(U^2) - \cancel{E^2(U)} \\
    &= E(U^2) \\
    &= \int_{-\infty}^{+\infty}\frac{u^2}{\sqrt{2\cdot\pi}} \cdot e^{-\frac{u^2}{2}}\cdot du\\
    &= \frac{2}{\sqrt{2 \cdot \pi}} \cdot \int_0^{+\infty}u^2 \cdot e^{-\frac{u^2}{2}} \cdot du  &&
    \text{(Car la fonction est paire)}
\end{align*}

Posons,
\begin{align*}
    t &= \frac{u^2}{2} \\
    \Rightarrow dt &= u \cdot du \\
    \Rightarrow du &= \frac{dt}{u} = \frac{dt}{\sqrt{2\cdot t}}
\end{align*}

En appliquant le changement de variable on obtient,
\begin{align*}
    V(U) &= \frac{2}{\sqrt{2\cdot\pi}}\cdot\int_0^{+\infty}2\cdot t \cdot e^{-t}\frac{dt}{\sqrt{2 \cdot t}} \\
    &= \frac{2}{\sqrt{\pi}}\cdot\int_0^{+\infty}t^{\frac{1}{2}} \cdot e^{-t} \cdot dt \\
    &= \frac{2}{\sqrt{2\cdot\pi}} \cdot \int_0^{+\infty}t^2 \cdot e^{-t} \cdot dt \\
    &= \frac{\cancel{2}}{\sqrt{\pi}} \cdot \frac{1}{\cancel{2}} \cdot \Gamma(\frac{1}{2}) \\
    &= \frac{\Gamma(\frac{1}{2})}{\sqrt{\pi}} \\
    &= \frac{\cancel{\sqrt{\pi}}}{\cancel{\sqrt{\pi}}} \\
    V(U) &= 1
\end{align*}

\end{preuve}



\subsection{Calcul des moments}

\begin{align*}
\mu_k &= E(U^k) \\
&= \int_{-\infty}^{+\infty}U^k \cdot f(u) \cdot du \\
&= \int_{-\infty}^{+\infty}U^k \frac{1}{\sqrt{2\pi}}e^{-\frac{U^2}{2}}du && U \nearrow L.G~(0, 1)
\end{align*}

\paragraph{}
Calculons les moments dans le cas général. Observons le comportement de $\mu_k$ en fonction de la parité de $k$
\subparagraph{$k$ impair}
\begin{equation*}
\mu_k = \int_{-\infty}^{+\infty}U^k \cdot \frac{1}{\sqrt{2\cdot\pi}}\cdot e^{-\frac{U^2}{2}} \cdot du = 0
\end{equation*}
Car $U^k \frac{1}{\sqrt{2\pi}}e^{-\frac{U^2}{2}}$ est impaire si $k$ est impair et on l'intègre sur le domaine $]-\infty, +\infty[$.


\subparagraph{$k$ pair}
\begin{align*}
\mu_{2k} &= E(U^{2k}) \\
&= \frac{1}{\sqrt{2\cdot\pi}}\cdot \int_{-\infty}^{+\infty}U^{2k} \cdot e^{-\frac{U^2}{2}} \cdot dU \\
&= \frac{2}{\sqrt{2\cdot\pi}}\cdot \int_0^{+\infty}U^{2k} \cdot e^{-\frac{U^2}{2}}\cdot dU
\end{align*}

Posons,
\begin{align*}
t &= \frac{U^2}{2} \\
\Rightarrow dt &= U\cdot dU \\
\Rightarrow U &= \sqrt{2\cdot t}
\end{align*}

On effectue le changement de variable,
\begin{align*}
\mu_{2k} &= \frac{2}{\sqrt{2\cdot\pi}}\cdot \int_0^{+\infty}(2\cdot t)^k\cdot e^{-t}\cdot\frac{dt}{\sqrt{2\cdot t}} \\
& = \frac{2^k}{\sqrt{\pi}}\cdot\int_0^{+\infty} t^{k - \frac{1}{2}} \cdot e^{-t} \cdot dt \\
&= \frac{2^k}{\sqrt{\pi}}\cdot \Gamma(k + \frac{1}{2}) \\
&= \frac{\cancel{2^k}}{\cancel{\sqrt{\pi}}} \cdot \frac{1\times 3\times 5\times\cdots\times(2\cdot k - 1)}
{\cancel{2^k}} \cdot\cancel{\Gamma(\frac{1}{2})} \\
&= 1\times 3\times 5\times\cdots\times(2k - 1) \\
&= \frac{(2k)!}{2\times 4\times 6\times 8\times\cdots\times 2k} \\
&= \frac{(2k!)}{2^kk!}
\end{align*}

Grâce à cette formule générale, on pourra par exemple calculer directement :
\begin{align*}
\mu_4 &= 3 \\
\mu_2 &= 1
\end{align*}
